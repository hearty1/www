# Pierwsza strona www

[cern](https://home.cern/science/computing/birth-web)

# Publikowanie strony

- zalozenie konta na gitlabie
- zalozenie projektu
- gitlab pages
- index.html

# Walidacja strony

- [walidator w3c](https://validator.w3.org/)
- znaczniki zamykajace [stackoverflow](https://stackoverflow.com/questions/24116546/stray-end-tag-img)

# stary html

- font
- center

# css

- czcionka, rozmiar (wzgledny, bezwzgledny), kolor
- obramowania
- rozmieszczanie elementow (divy, szerokosci, absolutne, wzgledne)
